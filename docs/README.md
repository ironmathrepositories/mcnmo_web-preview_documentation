# Документация по Модулю Предпросмотра

## Глобальные настройки

Инстанс компонента принимает необязательный параметр глобальных настроек `settings` вида:

```ts
interface IGlobalSettings {
    // способ центрирования модального окна попапа для попапов,
    // у которых способ центрирования не определён явно через `options.center`;
    // по умолчанию: 'screen'
    defaultPopupModalCenter: 'element' | 'screen';

    // способ центрирования модального окна попапа,
    // который переопределяет способ центрирования всех попапов;
    // по умолчанию: `null` = центрирование в зависимости от `options.center` попапа,
    //                `defaultPopupModalCenter`, если `options.center` не задан.
    forcePopupModalCenter: 'element' | 'screen' | null;
}
```

## Общая концепция

Модуль предпросмотра принимает на вход следующие данные:

`data` - это древовидная структура.

Общий формат данных выглядит так:

```ts
interface IElement {
    tag: ELEMENT_TAGS; // тип тега
    elem?: string | string[] | IElement | IElement[]; // содержимое текущего элемента
    options?: { // конфигурация текущего элемента
        label?: string, // название элементов полей ввода
        elementId?: string; // идентификатор элемента в структуре
        inputId?: string; // идентификатор поля ввода
        actionId?: string; // идентификатор элемента-действия
        actionType?: ACTION_TYPES; // тип элемента-действия
        attr?: Record<string, string>; // аттрибуты, совпадающие со спецификацией html.
        inputType?: INPUT_TYPES; // тип поля ввода
        keyboard?: Keyboard; // конфигурация редактора математических формул
        answerOptions?: IAnswerOptions; // параметры ответов полей ввода
        requiredInputs?: Array<string>; // обязательные к заполнению поля ввода для текущего элемента-действия
        relatedInputs?: Array<string>; // связанные с текущим элементом-действием поля ввода
        multiple?: boolean; // множественный выбор? Для выпадающих списков и полей ввода с возможностью множественного выбора
        [key: string]: unknown; // произвольные дополнительные поля
    };
}

enum ELEMENT_TAGS {
    P = 'p',
    PLAIN = 'plain',
    ROOT = 'root',
    SPAN = 'span',
    STRONG = 'strong',
    SUP = 'sup',
    TABLE = 'table',
    BR = 'br',
    TR = 'tr',
    MATH = 'math',
    IMAGE = 'image',
    EM = 'em',
    H1 = 'H1',
    H2 = 'H2',
    H3 = 'H3',
    H4 = 'H4',
    H5 = 'H5',
    H6 = 'H6',
    UL = 'ul',
    OL = 'ol',
    LI = 'li',
    POPUP = 'popup',
    SPOILER = 'spoiler',
    INPUT = 'input',
    ACTION = 'action'
}
 
enum INPUT_TYPES {
    INPUT = 'input',
    MATH_INPUT = 'mathInput',
    CHECKBOX = 'checkbox',
    RADIO = 'radio',
    RADIOBUTTON = 'radiobutton',
    SELECT = 'select',
    SORT = 'sort',
    MATCH = 'match'
}
 
enum ACTION_TYPES {
    BUTTON = 'button'
}
 
interface Keyboard {
    // интерфейс конфигурации редактора формул.
}

type AnswerOptions = AnswerOption[] | { [key: string]: AnswerOption[] }
```
Модуль предпросмотра парсит данный JSON, и на основе него строит иерархию react компонентов.  
Большинство интерфейсов элементов наследуются от описанного выше, если не указанно иное.

Есть три типа элементов которые можно получить из `elem.tag`:  
* элементы отображения контента (`src/components/elements`)
* элементы полей ввода (`src/components/mechanics/input_elements`)
* элементы отправки действий (`src/components/mechanics/action_elements`)

Для разных типов элеметов, в объекте `tag.options` могут встретиться специфические параметры.

#### Элементы отображения контента:  
* root_element - импортирует (`src/components/index.js`) и передает в него дочерние элементы `root` / сбрасывает стили

```ts
interface IRootElem {
    tag: 'root', // может находиться только на верхнем уровне
    elem: IElement | IElement[] // любой элемент кроме root
}
```

* math_element - отображает формулу, с помощью либы `@matejmazur/react-katex` [дока](https://github.com/MatejBransky/react-katex)

```ts
interface IMathElem {
    tag: 'math',
    options?: {
        display: 'inline' | 'block'
    },
    elem: string // только ТЕХ фрагмент закодированный в base64
}
```

* plain_element // пустой react fragment

```ts
interface IPlainElem {
    tag: 'plain',
    elem: string // листовой узел, не может иметь потомков
}
```

* heading_element

```ts
interface IHeadingElem {
    tag: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6',
    elem: string | string[] | IElement | IElement[] // строки и элементы сторокового контекста
}
```

* table_element

```ts
interface ITableElem {
    tag: 'table'
    options?: {
        bounds_top?: boolean, // граница таблицы сверху
        bounds_left?: boolean, // граница таблицы слева
        bounds_bottom?: boolean, // граница таблицы снизу
        bounds_right?: boolean, // граница таблицы справа
        bounds_internal?: boolean, // границы между ячейками таблицы
    },
    elem: Array<ITrElem>
}

interface ITrElem {
    tag: 'tr';
    elem: Array<ITdElem>;
}

interface ITdElem {
    tag: 'td',
    options?: {
        align?: TD_ALIGNMENTS
    },
    elem: string | string[] | IElement | IElement[] // любой элемент кроме root
}

enum TD_ALIGNMENTS {
    RIGHT = 'right',
    LEFT = 'left',
    CENTER = 'center'
}
```

* p_element

```ts
interface IPElement {
    tag: 'p',
    options?: {
        display?: 'inline' | 'block'
    },
    elem: string | string[] | IElement | IElement[] // сткроки и элементы строкового контекста
}
```

* span_element

```ts
interface ISpanElement {
    tag: 'span',
    options?: {
        display?: 'inline' | 'block'
    },
    elem: string | string[] | IElement | IElement[] // сткроки и элементы строкового контекста
}
```

* image_element

```ts
interface IImageElem {
    tag: 'image',
    options?: {
        src: string,
        alt: string
    }
    // не может иметь дочерних элементов
}
```

* list_elements

```ts
interface IListElem {
    tag: 'ul' | 'ol', 
    options?: {
        display?: 'inline' | 'block'
    },
    elem: Array<IlistItem>
}

interface IListItem {
    tag: 'li',
    elem: string | string[] | IElement | IElement[] // сткроки и элементы строкового контекста
}
```

* strong_element

```ts
interface IStrongElem {
    tag: 'strong',
    elem: string | string[] | IElement | IElement[] // сткроки и элементы строкового контекста
}
```

* br_element

```ts
interface IBrElem {
    tag: 'br',
    // не может иметь дочерних элементов
}

```
* em_element

```ts
interface IEmElem {
    tag: 'em',
    elem: string | string[] | IElement | IElement[] // сткроки и элементы строкового контекста
}
```

* sup_element

```ts
interface ISupElem {
    tag: 'sup',
    elem: string // листовой узел, не может иметь потомков
}
```

* spoiler_element

```ts
interface ISpoilerElem {
    tag: 'spoiler';
    options: {
        label: string; // label, отображаемый, когда спойлер закрыт
        labelOpened: string; // label, отображаемый, когда спойлер открыт
        content: string | string[] | IElement | IElement[];
    }
}
```

* popup_element

```ts
interface IPopupElem {
    tag: 'popup';
    elem: string | string[] | IElement | IElement[];
    options: {
        label: string; // заголовок попапа
        content: string | string[] | IElement | IElement[];

        // способ центрирования модального окна попапа:
        //  'element' — центрирование относительно элемента `IPopupElem.elem`
        //  'screen` - центрирование относительно экрана
        // если не передан — используется значение глобальных настроек `defaultPopupModalCenter`
        center?: 'screen' | 'element';
    }
}
```

#### Элементы полей ввода

Для элементов полей ввода, добавляються дополнительные поля в объект `options`:

##### input  
`options.inputId` - уникальный id поля ввода  
`options.type` - тип поля ввода  

```ts
interface IInput {
    tag: 'input',
    options: {
        inputId: string
        type: 'input'
    }
}
```

##### checkbox_group
`options.answerOptions` - объект с вариантами ответов
`options.answerOptions.label` - значение отображаемое пользователю
`options.answerOptions.value` - значение передаваемое на сервер для проверки

```ts
interface IAnswerOptions {
    answerId: 'уникальный id в рамках одной механики';
    value: string | string[];
    label: string | IElement // в корне ожидается тег 'p', в качестве дочерних элементов можно использовать только math/image/plain (в случае, если переданы другие элементы, фронт выдаст сообщение об ошибке, в остальном действуют те же правила)
}

interface ICheckbox {
    tag: 'input';
    options: {
        inputId: string;
        type: 'checkbox';
        answerOptions: [
            IAnswerOptions,
            ...
        ]
    }
}
```

##### radio_btn_group

```ts
interface IRadiobutton {
    tag: 'input',
    options: {
        inputId: string,
        type: 'radiobutton',
        answerOptions: Array<IAnswerOptions>
    }
}
```

##### math_input

Поле математической клавиатуры использует [данный пакет](https://bitbucket.org/ironmathrepositories/mcnmo_math-editor_npm/src/master/).

```ts
interface IMathInput {
    tag: 'input',
    options: {
        inputId: string
        type: 'mathInput',
        keyboard: Record<string, unknown> // объект параметров экранной клавиатуры (подробнее в доке mcnmo_math-editor_npm)
    }
}
```

##### popup

Кастомный компонент dropdown.

`options.subType` - подтип поля ввода, в данном случае множественный выбор или еденичный

```ts
interface IPopup extends ICommonData {
    tag: 'input',
    options: {
        inputId: string,
        type: 'popup',
        subType: 'single' | 'multiple',
        answerOptions: Array<IAnswerOptions>
    }
}
```

##### dynamicInput

Элемент, содержащий произвольное, изменяемое количество полей ввода одного типа. `options.type` - тип полей ввода: `dynamicInput | dynamicMathInput`.

```ts
interface IDynamicInput extends ICommonData {
    tag: 'input',
    options: {
        inputId: string,
        type: 'dynamicInput' | 'dynamicMathInput',
        // начальное количество отображаемых полей ввода, может быть нулём или отсутствовать
        initialAnswersCount?: number;

        // максимальное количество полей ввода, которые может добавить пользователь,
        // может отсутствовать, что означает потенциально неограниченное количество
        maxAnswersCount?: number;

        // текст, отображаемый при выборе варианта "Решений нет"
        noSolutionText?: string;

        // текст, отображаемый при неуказанном правильном ответе
        missingSolutionText?: string;

        // Формат клавиатуры, если type == 'mathInput'
        keyboard?: IMathInputKeyboard;
    }
}
```

##### match

Компонент, реализующий механику сопоставления элементов одной группы с элементами другой группы. Элемент одной группы может быть связан с любым количеством элементов второй группы.

```ts
interface MatchInput {
    tag: 'input',
    options: {
        inputId: string;
        type: 'match';
        colors?: Array<string>; // список цветов линий, default: ['black']
        scrollable?: boolean; // ограничен ли контейнер с ответами по высоте, default: false
        maxHeight?: number; // максимальная высота контейнера при `scrollable=true`, default: 420
        leftGroupLabel?: IWebPreview | string; 
        rightGroupLabel?: IWebPreview | string; 
        answerOptions: {
            leftGroup: Array<MatchItem>;
            rightGroup: Array<MatchItem>;
        }
    }
}

interface MatchItem {
    answerId: string;
    label: IWebPreview | string; // поддержка формул, изображения и текста
    value: string;
}

```

Формат ответа:

```ts
interface MatchAnswer {
    [inputId: string]: Array<MatchRelation>;
}

type MatchRelation = [ string | Array<string>, string | Array<string> ];
```


Пример ответа:

```json
{
    "dataInputID1": [
        [ "value11", "value21" ],
        [ "value11", "value23" ],
        [ "value12", "value22" ],
        [ "value12", "value23" ],
        [ "value13", "value22" ]
    ]
}
```

Формат результатов проверки ответа:

```ts
interface ICheckAnswer {
    actionId: string;
    checkResult: {
        [dataInputId: string]: {
            status: boolean;
            detailed: Array<[ [ string, string ], boolean ]>;
        };
    },
    replace?: {
        [elementId: string]: IWebPreview;
    }
}
```

Пример результата проверки:

```json
{
    "actionId": "dataActionID",
    "checkResult": {
        "dataInputID1": {
                    "status": false,
                    "detailed": [
                        [ [ "value11", "value21" ], false ],
                        [ [ "value12", "value22" ], true ],
                        [ [ "value12", "value23" ], true ],
                        [ [ "value11", "value22" ], true ]
                    ]
                }
    },
    "replace": {
        "elementID1": {
            "tag": "p",
            "elem": { ... }
        }
    }
}
```

##### Графические механики

Документация по графическим механикам расположена в [отдельном документе](./graphics-mechanics/README.md).

### Интерфейсы хранения данных полей ввода:

`input` / `mathInput` / `radiobutton` / `popup(type: 'single')` - `{ dataInputID: 'string' }`  
`checkbox` / `popup(type: 'multiple')` - `{ dataInputID: ['value1', 'value2'] }`

### Интерфейс `checkAnswerHandler`

```ts
interface ICheckAnswerHandler {
    (answer: ICheckAnswer): Promise<ICheckAnswerRespones>;
}
```

Функция принимает объект с параметрами ответа (описано ниже) и реализует произвольную логику обработки ответа, может быть:

* отправка ответа на сервер,
* отправка в соседний фрейм, 
* отправка в форкер,
* или проверка ответа на стороне фронта принимающей системы,
* любая другая логика...

Функция должна вернуть Promise объект с параметрами результатов проверки (описано ниже).

#### Элементы отправки действий

> `button` - кнопка для проверки ответов

Кнопка для проверки ответов вызывает callback `checkAnswerHandler`, в который,  
мы передаем необходимые данные из модуля предпросмотра, для отправки на сервер.  

```ts
// IAction описывает формат получения данных о действиях и связанных с действием полей
interface IAction {
    tag: 'action',
    options: {
        actionId: string // уникальный id интерфейса действия
        actionType: 'button',
        relatedInputs: Array<string>,
        requiredInputs: Array<string>, // список id полей, обязательных к заполнению
        display: 'inline' | 'block'
    }
}
export type IMatchingAnswer = Array<[ string | string[], string | string[] ]>;
 
export type IAnswer = boolean | string | string[] | IMatchingAnswer;

// ICheckAnswer описывает формат передачи данных для проверки на сервер
interface ICheckAnswer {
    actionId: string,
    data: {
        [inputId: string]: IAnswer, // в зависимости от типа инпута, будет разный формат передачи
    }
}

// ICheckAnswerRespones описывает формат ответа от сервера, на запрос проверки ответов
interface ICheckAnswerRespones {
    actionId: string // уникальный id интерфейса действия
    checkResult: {
        [inputId: string]: boolean | boolean[] | "init"
    },// может быть пустым объектом
    replace: {
        [elementIdOrPath]: IElement // элемент дерева документа для вставки или замены
    }
}
```

ICheckAnswerRespones описывает состояние проверки. Формат ответа, по идее должен соответствовать формату хранения, но сейчас это не так `checkResult[inputId]` - всегда булевое значение. В будущем планируется реализовать массив булевых значения, чтобы можно было поддержать концепцию частично правильного ответа.

Все типы инпутов поддерживают значение `"init"` в поле `checkResult[inputId]`. В таком случае состояние соответствующего инпута после выполнения проверки будет нейтральным (идентичным начальному состоянию без проверки). 

Для инпутов, чьи данные хранятся в виде массива, так как мы не получаем данные о верности конкретного пункта, а только общее значение.

Динамические инпуты `IDynamicInput` поддерживают формат ответа с массивом булевых значений для отображения частично верного ответа. Так же `IDynamicInput` поддеживает ситуацию с существованием неуказанного ответа, когда все добавленные ответы верны. В таком случае `IDynamicInput` ожидает дополнительный `false` элемент в конц массива проверки ответов.

Поле `replace` отдает данные для замены структуры JSON, которую мы передаем в props `data` модуля предпросмотра. `replace` представляет из себя объект, где ключами являются либо уникальные `elementId` элементов, которые нужно заменить, либо пути до элементов. Если ключ начинается с `root.`, то он расценивается, как путь, иначе как `elementId`, поэтому нельзя использовать в качестве `elementId` строки, начинающиеся с `root.`.

Значениями объекта `replace` являются `IElement`, на которые будут заменены существующие элементы с соответствующими ключами:

```js
{
    replace: {
        'uniqueElementId': {
            tag: 'span',
            elem: 'I will replace element with `elementId` "uniqueElementId"',
        },
        'root.0.p.span.action': {
            tag: 'span',
            elem: 'I will replace element by path "root.0.p.span.action"',
        }
    }
}
```

#### Предзаполненное состояние интерактивных механик

На момент инициализации МПП, можно сразу передать начальное состояние любой (всех) интерактивных механик, включая состояние проверенности. Для этого необходимо в МПП при инициализации передать `initialInputsState` параметр. Интерфейс параметра:

```ts
interface IInitialInputsState {
    actions: Array<IActionState>; // состояния кнопок отправки ответа
    inputs: Array<IInputState>; // состояние интерактивных механик
}

interface IActionState {
    actionId: string;
    disabled?: boolean; // false по-умолчанию
}

interface IInputState {
    inputId: string;
    data?: IAnswer; // различный формат, в зависимости от типа мехники
    checkResult?: ICheckResult; // различный формат, в зависимости от типа механики, если не передан - назначается начальное состояние по-умолчанию
    disabled?: boolean; // false по-умолчанию
}

enum CheckResultStatusTypes {
    RIGHT = 'right',
    WRONG = 'wrong',
    INIT = 'init'
}
```

Передача этого параметра определяет состояние данных и проверки только при монтировании компонента МПП. Последующие изменения значения этого параметра на состояние данных в МПП - не влияет.

## Стилизация

Все стили условно можно разделить на 4 абстракции:

```js
const classes = classNames(
    'ComponentName', // глобальный класс, без css-modules
    styles.ComponentName, // внутренние стили модуля
    externalStyles.ComponentName, // внешний объект стилей
    getClasses(externalStyles, className), // getClasses оборачивает className в модули externalStyles
    className, // так же доступен глобальный класс, без css-modules
);
```
* внутренние стили модуля (css-modules);
    * внутренние стили изолированны на уровне пакета, может прописать только разработчик, внутри модуля предпросмотра.
* внешний объект стилей (css-modules) из принимающей системы;
    * внешний объект `externalStyles` (css-modules) из принимающей системы
* объект параметров конфигурации из jsonData;
    * параметры для стилизации, например: `options.display` или `options.align` (для `td`)
* кастомный класс (массив классов) для элемента из jsonData(options.attr.class)
    * доступна стилизация через внешний объект стилей externalStyles, или через глобальные классы

### Список имен классов для контейнеров элементов

При необходимости можно определить описанные ниже мена классов в глобальном css файле или в объекте стилей, передаеваемом на вход в `externalStyles`:

* RootElement
* EmElement
* HeadingElement
* ImageElement
* ListItemElement
* ListElement
* MathElement
* PElement
* SpanElement
* StrongElement
* TdElement
* TrElement
* TableElement
* SpoilerElement
* SpoilerELementLabel
* SpoilerElementContent
* SpoilerElementIcon
* PopupElementTriggerButton
* PopupElementModal
* PopupElementModalHeader
* PopupElementModalContent 
* ActionButton
* CheckboxGroup
* InputElement
* MathInputElement
* Popup
* RadiobuttonGroup
* MatchElement
* MatchElementGroup
* MatchElementGroupHeader
* MatchElementGroupItem
* MatchElementGroupItemFlag
* MatchElementGroupItemFlagBackground
* DynamicInputElement
* DynamicInputFieldList
* DynamicInputFieldWrapper
* DynamicInputControls
* DynamicInputAddAnswerButton
* DynamicInputNoSolutionButton
* DynamicInputNoSolutionMessage
* DynamicInputMissingSolution

#### Ссылки на документы:

[Интерфейсы match and choice -- НОВАЯ РЕАЛИЗАЦИЯ](https://docs.google.com/document/d/131rUA4qTEpaM-5V2zWU6nilENuJ3Y7WeYVzdjvgRS8I/edit?usp=sharing)  
[Первоначальный документ](https://docs.google.com/document/d/1pGC8VFslMcp4rbEKmKtRtQT0wq_GNPopr5haF3RAPWA/edit?usp=sharing)