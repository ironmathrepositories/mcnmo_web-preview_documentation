# Графическая механика

Интерактивный элемент, с возможностью как статичного отображения, так и с возможностью взаимодействия с пользователем и отправки результатов взаимодействия на проверку. В перспективе, спектр образовательных задач, решаемых с помощью этой механики, может быть довольно широким: от расстановки точек на графике, до задач по геометрии. Мы не будем пытаться весь функционал реализовать в одной механике. Напротив, будем постепенно вводить подтипы новых механик по мере поступления запроса на них.

Однако, все графические механики будут иметь одно основание: поле графика, с осями (или без них), с засечками на осях (или без них), с сеткой, подписями и пр. - все это регулируется входными параметрами настроек. Они описаны ниже.

Графическая механика имеет три слоя:

- слой координатной плоскости: здесь реализована сетка, оси, подписи к осям, насечки на осях

- слой статичных графических элементов: здесь могут быть размещены элементы, не предусматривающие взаимодействие с пользователем (график функции, просто как линия, например)

- интерактивный слой: здесь добавляются все те элементы (линии, точки, области), с которыми пользователь уже может взаимодействовать (кликать, двигать, добавлять и удалять) и формировать данные ответа, которые потом отправляются на сервер на проверку

Графику реализуем на базе svg формата.

## Общий формат

Интерактивная графическая механика - это стандартный элемент модуля предпросмотра:

```ts
interface IGraphics {
    tag: 'input'; // стандартное обозначение механики
    options: {
        inputId: string; // идентификатор механики
        disabled: boolean; // механика может быть заблокирована по флагу (пока просто предусмотреть)
        type: 'graphics'; // тип механики - графический интерактив
        subType: GraphicsSubTypes; // подтип графического интерактива
        initialState: IInitialState; // начальное состояние данных и элементов, ложится в основу state и формирует начальное пложение всех элементов на графике
        graphicsOptions: IGraphicsOptions; // параметры "полотна" графика (оси, сетка, подписи...)
        additionalElements: Array<IAdditionalElement>, // дополнительные статичные элементы на графике, без пподдержки интерактивности
        cursorProjection: { 
            showQ?: { // показывать ли проекции курсора на координатные оси?
                x?: boolean;
                y?: boolean;
            },
            showCoordinates?: { // показывать ли координаты проекции курсора?
                x?: boolean;
                y?: boolean;
            }
        }
    }
}

type IInitialState =
    IClickablePointsInitial |
    IMovablePointsInitial |
    IDynamicMovablePointsInitial |
    IMovablePointsLinesInitial |
    IDynamicMovablePointsLinesInitial |
    ILinesInitial;

interface IAdditionalElement {
    type: AdditionalElementTypes;
    options: IAdditionalElementsOptions;
}

enum AdditionalElementTypes {
    TEXT = 'text',
    CIRCLE = 'circle',
    SECTOR = 'sector',
    RECT = 'rect',
    LINE = 'line',
    ARROW = 'arrow',
    POLYLINE = 'polyline',
    POLYGON = 'polygon',
    PATH = 'path',
    IMAGE = 'image'
}

type IAdditionalElementsOptions = 
    IText |
    ICircle |
    ISector |
    IRect |
    ILine |
    IPolyline |
    IPolygon |
    IPath |
    IImage;

enum GraphicsSubTypes {
    ClickablePoints = 'clickablePoints',
    MovablePoints = 'movablePoints', // необходимо в первую очередь
    DynamicMovablePoints = 'dynamicMovablePoints',
    MovablePointsLines = 'movablePointsLines',
    DynamicMovablePointsLines = 'dynamicMovablePointsLines',
    Lines = 'lines',
    LabeledGraphics = 'labeledGraphics'
}

interface IGraphicsOptions {
    coordinateSystem: 'decart' | 'affine' | 'polar';
    axesOrigin?: [ number, number ]; // значения, на которых оси пересекаются
    originLabel?: { // подпись точки пересечения осей
        pos: [ 'left' | 'right', 'bottom' | 'top' ];
        type?: 'plain' | 'base64'; // тип подписи — простая строка или формула
        text: string;
        offset?: [ number, number ]; // вектор смещения в математических единицах
        color?: string;
        fontSize?: number;
        italicQ?: boolean;
        boldQ?: boolean;
    },
    roundingCoordinates?: number | IRoundingCoordinates | null; // шаг сетки (невидимой), к которым привязываются точки
    exportQ?: boolean; // показывать ли кнопку для скачивания SVG
    zoomQ?: boolean; // разрешать ли зум графика
    zoomControls?: {
        position?: [ 'left' | 'right', 'top' | 'bottom' ]; // позиция панели управления зумом
        maxZoom?: number; // максимальный уровень зума, должен быть больше или равен 1
        zoomStep?: number; // шаг зума, где 1 = двукратное увеличение
    },
    axes?: { // настройки осей графика
        generalViewOptions: { // общие настройки
            show?: boolean; // показывать или нет?
            thickness?: number; // толщина линий
            color?: string; // цвет
            // коэффициент длины и ширины стрелки
            // итоговый размер будет получен умножением коэффициента `arrowHeadSize` на параметр `thickness`
            arrowHeadSize?: [number, number]; 
        },
        x?: { // директивная настройка оси X, переопределяет значения из generalViwOptions
            show?: boolean;
            thickness?: number;
            color?: string;
            arrowHeadSize?: [number, number]; // коэффициент длины и ширины стрелки
        },
        y?: { // директивная настройка оси Y, переопределяет значения из generalViwOptions
            show?: boolean;
            thickness?: number;
            color?: string;
            arrowHeadSize?: [number, number]; // коэффициент длины и ширины стрелки
        }
    },
    axesLabels: { // подписи к осям
        generalViewOptions: {
            color?: string; // цвет
            fontSize?: number; // размер шрифта
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        },
        x?: { // подпись к оси Х
            pos: 'topCenter' | 'topRight' | 'bottomCenter' | 'bottomRight' | 'right'; // расположение подписи
            text: { // настройки текста подписи
                type?: 'plain' | 'base64';
                label: string; // текст подписи
                offset?: [ number, number ];
                color?: string; // цвет
                fontSize?: number; // размер шрифта
                italicQ?: boolean; // курсив?
                boldQ?: boolean; // жирный?
            }
        },
        y?: {
            pos: 'top' | 'topRight' | 'topLeft' | 'centerLeft' | 'centerRight';
            rotate: 'horizontal' | 'up' | 'down';
            text: {
                type?: 'plain' | 'base64';
                label: string;
                offset?: [ number, number ];
                color?: string;
                fontSize?: number;
                italicQ?: boolean;
                boldQ?: boolean;
            }
        }
    },
    axesTicks: { // насечки на осях
        generalViewOptions?: { // общие настройки насечек
            tick?: { // общие настройки самих засечек
                type?: 'dot' | 'dash' | 'emptyCircle'; // точка (или закрашенный кружок), черточка, пустой кружок
                options: IDotTickOptions | IDashTickOptions | IEmptyCircleTickOptions;
            },
            label?: { // общие настройки надписей к засечкам
                color?: string; // цвет
                fontSize?: number; // размер шрифта
                italicQ?: boolean; // курсив?
                boldQ?: boolean; // жирный?
            }
        },
        x: Array<{ // насечки на оси Х
            list: Array<{ // список значений насечек на оси и их подписи
                pos: number;
                label?: {
                    pos?: 'top' | 'bottom'; // позиция подписи относительно оси
                    type?: 'plain' | 'base64';
                    text: string;
                    offset?: [ number, number ];
                }
            }>;
            tick?: {
                type?: 'dot' | 'dash' | 'emptyCircle';
                options: IDotTickOptions | IDashTickOptions | IEmptyCircleTickOptions;
            },
            label?: {
                pos?: 'top' | 'bottom'; // позиция относительно оси по умолчанию для всех подписей насечек в list
                type?: 'plain' | 'base64';
                text: string;
                offset?: [ number, number ]; // смещение по умолчанию для всех подписей насечек в list
                color?: string;
                fontSize?: number;
                italicQ?: boolean;
                boldQ?: boolean;
            }
        }>,
        y: Array<{ // насечки на оси Y
            list: Array<{ // список значений насечек на оси и их подписи
                pos: number;
                label?: {
                    pos?: 'left' | 'right'; // позиция подписи относительно оси
                    type: 'plain' | 'base64';
                    text: string;
                    offset?: [ number, number ];
                }
            }>;
            tick: {
                type?: 'dot' | 'dash' | 'emptyCircle';
                options: IDotTickOptions | IDashTickOptions | IEmptyCircleTickOptions;
            },
            label?: {
                pos?: 'left' | 'right'; // позиция относительно оси по умолчанию для всех подписей насечек в list
                type?: 'plain' | 'base64';
                text: string;
                offset?: [ number, number ]; // смещение по умолчанию для всех подписей насечек в list
                color?: string;
                fontSize?: number;
                italicQ?: boolean;
                boldQ?: boolean;
            }
        }>,
    },
    gridLines: { // настройки видимой сетки
        generalViewOptions: { // общие настройки линий сетки
            color?: string; // цвет
            thickness?: number; // толщина линии
            type?: 'dotted' | 'dashed' | 'line'; // тип линии
        },
        x: Array<{ // от оси Х
            pos: Array<number>; // положение линии
            options: { // параметры данной конкретной линии
                color?: string; // цвет 
                thickness?: number; // толщина
                type?: 'dotted' | 'dashed' | 'line'; // тип
            }
        }>,
        y: Array<{ // от оси Y
            pos: Array<number>;
            options: {
                color?: string;
                thickness?: number;
                type?: 'dotted' | 'dashed' | 'line';
            }
        }>
    },
    plotRange: { // диапазон графика по осям
        x: [ number, number ];
        y: [ number, number ];
    },
    background: string; // цвет фона
    padding: [ number, number, number, number ] | [ number, number, number ] | [ number, number ]; // внутренние отступы, стандартный формат
    aspectRatio?: number; // соотношение координатных шкал друг-другу (сжатие-растяжение графика)
    contentSize: number | [ number, number ]; // размер контентной части (графика) изображения
    graphicsLabel: { // подпись к графику внизу и/или вверху
        top?: {
            type?: 'plain' | 'base64';
            text: string;
            offset?: [ number, number ]; 
            color?: string;
            fontSize?: number;
            italicQ?: boolean;
            boldQ?: boolean;
        }
        bottom?: {
            type?: 'plain' | 'base64';
            text: string;
            offset?: [ number, number ];
            color?: string;
            fontSize?: number;
            italicQ?: boolean;
            boldQ?: boolean;
        }
    }
}

interface IDotTickOptions {
    color?: string;
    radius?: number;
}

interface IDashTickOptions {
    color?: string;
    width?: number;

    // Длина засечки
    //   number - одинаковая длина по обе стороны от оси
    //   [number, number] - в зависимости от ориентации оси, на которой находится засечка:
    //     для горизонтальной оси - [длина снизу, длина сверху]
    //     для вертикальной оси - [длина слева, длина справа]
    length?: number | [number, number];
}

interface IEmptyCircleTickOptions {
    color?: string;
    radius?: number;
    borderWidth?: number;
}

interface IRoundingCoordinates {
    x: number | Array<number>;
    y: number | Array<number>;
}
```

### subType: подтип графической механики

Поле `options.type` всегда содержит значение `graphics`, а поле `subType` указывает на подтип механики. Начальный набор подтипов графической механики описан в `GraphicsSubTypes` enum объекте. Это не все подтипы, которые нужны вообще, но те, с которых необходимо начать. Каждый подтип предоставляет следующую функциональность:

- `ClickablePoints` - это график со статично расположенными точками, каждая точка имеет свой идентификатор и состояние true/false; точка кликабельна, и по клику переключает свое состояние. Такой тип графика может быть полезен для задач вида: есть график (статичный), на графике разбросаны точки (видны явно) и нужно выбрать те, которые отражают локальный/глобальный максимумы функции. Формируется массив булевых значений.

- `MovablePoints` - это график с предзаданным набором точек, которые нужно правильно расставить на плоскости, порядок расстановки значения не имеет. По итогам работы с таким графиком формируется массив координат, который и отправляются на сервер для проверки.

- `DynamicMovablePoints` - аналогично динамическим инпутам, график предоставляет возможность добавлять и удалять (по двойному клику) точки и размещать их в нужных узлах. Формируется массив координат, переменной длинны, и отправляется на проверку.

- `MovablePointsLines` - график с фиксированным набором подвижных точек соединенных линией (не замкнутой). Нужно расставить точки в узлах координатной сетки правильным образом. Формируется массив координат, последовательность координат имеет значение.

- `DynamicMovablePointsLines` - аналогично динамическим инпутам и `DynamicMovablePoints` - набор подвижных точек, соединенных линией, переменной длины. Можно добавлять точки в конец, удалять любую подвижную (детали ниже). Формируется массив координат, в последовательность координат имеет значение.

- `Lines` - механика для построения отрезков, прямых и лучей по двум точкам с возможностью добавлять и удалять точки.

- `LabeledGraphics` - маханика статичных графических элементов с интерактивными подписями, проставляемыми пользователем.

Потенциально список подтипов графических интерактивов может расти бесконечно - просто добавляется новый подтип и соответствующий компонент в модуле предпросмотра. Могут появиться задачи геометрические, с полигонами, которые кликабельны или можно двигать узловые точки полигона, или можно добавлять удалять точки полигона, геометрические, тригонометрические задачи и так далее...

Каждый подтип механики определяет своей интерфейс элементов и их параметров, при этом каждый подтип формирует массив однотипных элементов графика (если не предусмотрено логикой иное).

### initialState: форматы начальных состояний графических интерактивов

Ниже идут описания поля `options.initialState` для каждого подтипа графической механики.

#### ClickablePoints

Начальное состояние описывает массив фиксированных кликабельных точек на графике:

```ts
interface IClickablePoint {
    id: string; // идентификатор точки
    viewOptions: { // параметры внешнего вида
        label?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            type: 'plain' | 'base64' // тип подписи — простая строка или формула
            text: string; // текст подписи
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        },
        point: { // настройки внешнего вида самой точки
            checked: {
                color: string;
                radius: number;
            },
            unchecked: {
                color: string;
                radius: number;
            },
            correct?: { // когда точка является правильным ответом
                color?: string;
                radius?: number;
            }
            incorrect?: { // когда точка является неправильным ответом
                color?: string;
                radius?: number;
            }
        }
    },
    projection?: { // проекции точки на оси
        x?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false; // `false` отключает отображение проекции для конкретной точки, если задано отображение проекций для всех точек в `generalViewOptions`
        y?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false;
    },
    pos: [ number, number ] // координаты точки
}

interface IClickablePointsInitial {
    generalViewOptions: { // общие параметры отображения для всех точек массива
        label?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        },
        point: { // настройки внешнего вида самой точки
            checked?: {
                color?: string; // цвет фона
                radius?: number; // радиус "точки"
            },
            unchecked?: {
                color?: string;
                radius?: number;
            },
            correct?: { // когда точка является правильным ответом
                color?: string;
                radius?: number;
            }
            incorrect?: { // когда точка является неправильным ответом
                color?: string;
                radius?: number;
            }
        },
        projection?: { // проекции точки на оси
            x?: {
                color?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line';
            },
            y?: {
                color?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line';
            }
        }
    },
    points: Array<IClickablePoint>;
}
```

Передаётся массив точек и их координат. Поле `generalViewOptions` определяет общий вид для всех точек, если же в какой-то точке определены свои параметры внешнего вида - то они переопределяют настройки из `generalViewOptions`. Так можно отдельные точки выделять особым цветом, подписью. Изначально формируется массив из `false` значений, который потом по клику соответствующей точки переводится в состояние `true`. Порядок значений соотносится с порядком точек в списке.

Данный элемент в будущем, возможно, будет расширен и преобразован в `ClickableElements`, который будет принимать массив кликабельных элементов графика (не только точки, но и линии, в том числе кривые, полигоны и пр.).

Объект ответа `checkAnswer.data` представляет собой массив булевых значений:

```ts
interface ICheckAnswer {
    actionId: string;
    data: {
        [inputGraphicsId: string]: Array<boolean>; // массив булевых значений
    }
}
```

#### MovablePoints

Фиксированный набор точек, которые можно перемещать по координатной плоскости. Начальное состояние описывает вид точек и их начальные положения:

```ts
interface IMovablePoint {
    id: string; // идентификатор точки
    isMovable: boolean; // можно ли точку двигать?
    viewOptions: { // параметры внешнего вида
        label?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            type: 'plain' | 'base64';
            text: string; // текст подписи
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        },
        point: { // настройки внешнего вида самой точки
            background?: string; // цвет фона
            radius?: number;
            correct?: { // когда точка является правильным ответом
                background?: string;
                radius?: number;
            }
            incorrect?: { // когда точка является неправильным ответом
                background?: string;
                radius?: number;
            }
        }
    },
    projection?: { // проекции точки на оси
        x?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false; // `false` отключает отображение проекции для конкретной точки, если задано отображение проекций для всех точек в `generalViewOptions`
        y?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false;
    },
    pos: [ number, number ] // координаты точки
}

interface IMovablePointsInitial {
    generalViewOptions: { // общие параметры отображения для всех точек массива
        label?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        },
        point: { // настройки внешнего вида самой точки
            background?: string; // цвет фона
            radius?: number; // радиус "точки"
            correct?: { // когда точка является правильным ответом
                background?: string;
                radius?: number;
            }
            incorrect?: { // когда точка является неправильным ответом
                background?: string;
                radius?: number;
            }
        },
        projection?: { // проекции точки на оси
            x?: {
                color?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line';
            },
            y?: {
                color?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line';
            }
        }
    },
    points: Array<IMovablePoint>;
}
```

Структура полностью повторяет таковую из `ClickablePoints`, добавляется только параметр `isMovable`: точка может быть "заморожена", её состояние в итоговом массиве координат останется неизменным.

На выходе получаем массив координат точек, порядок не имеет значение, главное чтобы точки в принципе были расставлены по правильным местам.

Объект ответа `checkAnswer.data` представляет собой массив пар чисел:

```ts
interface ICheckAnswer {
    actionId: string;
    data: {
        [inputGraphicsId: string]: Array<[ number, number ]>; // массив координат, пар чисел
    }
}
```

Массив координат, должен быть равен длине массива исходных точек.

#### DynamicMovablePoints

Массив подвижных точек, переменной длины. Пользователь здесь может сам дополнить точки или удалить ранее проставленные:

```ts
interface IDynamicMovablePoint {
    id: string; // идентификатор точки
    isMovable: boolean; // можно ли точку двигать?
    viewOptions: { // параметры внешнего вида
        label?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            type: 'plain' | 'base64';
            text: string; // текст подписи
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        },
        point: { // настройки внешнего вида самой точки
            background?: string; // цвет фона
            radius?: number;
            correct?: { // когда точка является правильным ответом
                background?: string;
                radius?: number;
            }
            incorrect?: { // когда точка является неправильным ответом
                background?: string;
                radius?: number;
            }
        }
    },
    projection?: { // проекции точки на оси
        x?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false; // `false` отключает отображение проекции для конкретной точки, если задано отображение проекций для всех точек в `generalViewOptions`
        y?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false;
    },
    pos: [ number, number ] // координаты точки
}

interface IDynamicMovablePointsInitial {
    maxCount: number; // максимальное количество точек
    actions?: {
        addPoint?: 'Click' | 'DoubleClick'; // Тип триггера действия добавления точки
        removePoint?: 'Click' | 'DoubleClick'; // Тип триггера действия удаления точки
    },
    generalViewOptions: { // общие параметры отображения для всех точек массива
        label?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        },
        point: { // настройки внешнего вида самой точки
            background?: string; // цвет фона
            radius?: number; // радиус "точки"
            correct?: { // когда точка является правильным ответом
                background?: string;
                radius?: number;
            }
            incorrect?: { // когда точка является неправильным ответом
                background?: string;
                radius?: number;
            }
        },
        projection?: { // проекции точки на оси
            x?: {
                color?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line';
            },
            y?: {
                color?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line';
            }
        }
    },
    points: Array<IDynamicMovablePoint>;
}
```

Интефейс начального состояния полностью повторяет таковой для `MovablePoints`. Отличия только следующие:

- начальное состояние может быть пустым,

- поле `isMovable` может иметь значение `false` только для предзаданных точек, для всех вновь созданных - он всегда равен `true`. Точки с `isMovalbe=false` нельзя удалить, они заморожены, но учитываются в финальном ответе.

На выходе, аналогично `MovablePoints`, получаем массив координат точек, порядок которого не имеет значения, главное чтобы в принципе были перечислены правильно все позиции.

Точки добавляются на коодинатную плоскость кликом или двойным кликом, таким же образом удаляются.

Объект ответа `checkAnswer.data` представляет собой массив пар чисел:

```ts
interface ICheckAnswer {
    actionId: string;
    data: {
        [inputGraphicsId: string]: Array<[ number, number ]>; // массив координат, пар чисел
    }
}
```

Массив с координатами, имеет произвольную длину.

#### MovablePointsLines

Фиксированный набор точек, соединенных линией, и которые можно двигать. У точки и линии могут быть подписи, на выходе важна последовательность полученных координат. График реализован на основе `polyline` svg примитива. Входные данные реализуют интерфейс:

```ts
interface IMovablePointLine {
    id: string; // идентификатор точки
    isMovable: boolean; // можно ли точку двигать?
    viewOptions: { // параметры внешнего вида
        pointLabel?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            type: 'plain' | 'base64';
            text: string; // текст подписи
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        },
        lineLabel?: { // настройки подписи на линии ДО точки. То есть параметр передаётся, если это необходимо, для точки с индексом 1 и далее
            position?: 'fixed' | 'parallel'; // текст параллельно линии или "сам по себе"
            offset?: { // смещение позиции подписи (верхнего левого угла подписи) относительно середины линии
                radius: number; // расстояние от середины линии до верхнего левого угла элемента с текстом
                angle: number; // угол, "азимут"
            },
            text: string;
            fontSize?: number;
            color?: string;
            italicQ?: boolean;
            boldQ?: boolean;
        },
        point: { // настройки внешнего вида самой точки
            background?: string; // цвет фона
            radius?: number;
            correct?: { // когда точка является правильным ответом
                background?: string;
                radius?: number;
            }
            incorrect?: { // когда точка является неправильным ответом
                background?: string;
                radius?: number;
            }
        }
    },
    projection?: { // проекции точки на оси
        x?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false; // `false` отключает отображение проекции для конкретной точки, если задано отображение проекций для всех точек в `generalViewOptions`
        y?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false;
    },
    pos: [ number, number ] // координаты точки
}

interface IMovablePointsLinesInitial {
    generalViewOptions: { // общие параметры отображения для всех точек массива
        pointLabel?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        },
        lineLabel?: { // настройки подписи на линии ДО точки. То есть параметр передаётся, если это необходимо, для точки с индексом 1 и до конца.
            position?: 'fixed' | 'parallel'; // текст параллельно линии или "сам по себе"
            offset?: { // смещение позиции подписи (верхнего левого угла подписи) относительно середины линии
                radius: number; // расстояние от середины линии до верхнего левого угла элемента с текстом
                angle: number; // угол, "азимут"
            },
            fontSize?: number;
            color?: string;
            italicQ?: boolean;
            boldQ?: boolean;
        },
        line: { // график реализован на основе polyline, поэтому параметры линии задаются один раз в generalViewOptions
            color?: string;
            thickness?: number;
            type?: 'line' | 'dotted' | 'dashed';
        },
        point: { // настройки внешнего вида самой точки
            background?: string; // цвет фона
            radius?: number; // радиус "точки"
            correct?: { // когда точка является правильным ответом
                background?: string;
                radius?: number;
            }
            incorrect?: { // когда точка является неправильным ответом
                background?: string;
                radius?: number;
            }
        },
        projection?: { // проекции точки на оси
            x?: {
                color?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line';
            },
            y?: {
                color?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line';
            }
        }
    },
    points: Array<IMovablePointLine>;
}
```

Здесь важно отметить, что линия задаётся через `polyline` примитив поэтому не протяжении всего графика имеет одинаковый стиль, и этот стиль задаётся один раз в объекте `generalViewOptions.line`. Узлы `polyline` объекта совпадают с координатами точек.

Точки напротив, могут быть выделены цветом, размером, и каждая из них может иметь свои индивидуальные настроки отображения.

Подписи на линии передаются в объекте с параметрами точки, и эта подпись располагается на линии посередине между точками в кружке. Надпись ставится на линию **ДО** указанной точки.

На выходе, для проверки ответа, мы получаем массив с координатами точек-узлов линии. Порядок массива здес имеет значение, узлы должны быть расположены в правильных координатах в правильной последовательности.

Объект ответа `checkAnswer.data` представляет собой массив пар чисел, связанный с идентификатором точки:

```ts
interface ICheckAnswer {
    actionId: string;
    data: {
        [inputGraphicsId: string]: Array<{
            id: string; // идентификатор точки
            pos: [ number, number ]; // позиция точки
        }>;
    }
}
```

Массив с ответом, должен быть равень длине исходного массива точек.

#### DynamicMovablePointsLines

Объект полностью аналогичен объекту `MovablePointsLines`. Разница только в том, что неподвижные `isMovable=false` узлы нельзя удалять, и начальное состояние может быть пустым. Новые узлы добавляются кликом или двойным кликом в конец списка. Таким же образом удаляются. Удалить можно любую `isMovable=true` точку-узел в любое время.

```ts
interface IDynamicMovablePointLine {
    id: string; // идентификатор точки
    isMovable: boolean; // можно ли точку двигать?
    viewOptions: { // параметры внешнего вида
        pointLabel?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            type: 'plain' | 'base64';
            text: string; // текст подписи
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        },
        lineLabel?: { // настройки подписи на линии ДО точки. То есть параметр передаётся, если это необходимо, для точки с индексом 1 и далее
            position?: 'fixed' | 'parallel'; // текст параллельно линии или "сам по себе"
            offset?: { // смещение позиции подписи (верхнего левого угла подписи) относительно середины линии
                radius: number; // расстояние от середины линии до верхнего левого угла элемента с текстом
                angle: number; // угол, "азимут"
            },
            type: 'plain' | 'base64';
            text: string;
            fontSize?: number;
            color?: string;
            italicQ?: boolean;
            boldQ?: boolean;
        },
        point: { // настройки внешнего вида самой точки
            background?: string; // цвет фона
            radius?: number;
            correct?: { // когда точка является правильным ответом
                background?: string;
                radius?: number;
            }
            incorrect?: { // когда точка является неправильным ответом
                background?: string;
                radius?: number;
            }
        }
    },
    projection?: { // проекции точки на оси
        x?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false; // `false` отключает отображение проекции для конкретной точки, если задано отображение проекций для всех точек в `generalViewOptions`
        y?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false;
    },
    pos: [ number, number ] // координаты точки
}

interface IDynamicMovablePointsLinesInitial {
    actions?: {
        addPoint?: 'Click' | 'DoubleClick'; // Тип триггера действия добавления точки
        removePoint?: 'Click' | 'DoubleClick'; // Тип триггера действия удаления точки
    },
    generalViewOptions: { // общие параметры отображения для всех точек массива
        pointLabel?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        },
        lineLabel?: { // настройки подписи на линии ДО точки. То есть параметр передаётся, если это необходимо, для точки с индексом 1 и до конца.
            position?: 'fixed' | 'parallel'; // текст параллельно линии или "сам по себе"
            offset?: { // смещение позиции подписи (верхнего левого угла подписи) относительно середины линии
                radius: number; // расстояние от середины линии до верхнего левого угла элемента с текстом
                angle: number; // угол, "азимут"
            },
            fontSize?: number;
            color?: string;
            italicQ?: boolean;
            boldQ?: boolean;
        },
        line: { // график реализован на основе polyline, поэтому параметры линии задаются один раз в generalViewOptions
            color?: string;
            thickness?: number;
            type?: 'line' | 'dotted' | 'dashed';
        },
        point: { // настройки внешнего вида самой точки
            background?: string; // цвет фона
            radius?: number; // радиус "точки"
            correct?: { // когда точка является правильным ответом
                background?: string;
                radius?: number;
            }
            incorrect?: { // когда точка является неправильным ответом
                background?: string;
                radius?: number;
            }
        },
        projection?: { // проекции точки на оси
            x?: {
                color?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line';
            },
            y?: {
                color?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line';
            }
        }
    },
    points: Array<IDynamicMovablePointLine>;
}
```

Объект ответа `checkAnswer.data` представляет собой массив пар чисел, связанный с идентификатором точки:

```ts
interface ICheckAnswer {
    actionId: string;
    data: {
        [inputGraphicsId: string]: Array<{
            id: string; // идентификатор точки
            pos: [ number, number ]; // позиция точки
        }>;
    }
}
```

#### Lines

Механика для построения отрезков, прямых и лучей по двум точкам с возможностью динамически добавлять и удалять точки и построенные по ним линии. Линия строится по двум последовательно добавленным точкам — при добавлении одной точки линия не строится, ожидается добавление второй точки. Удаление точки удаляет линию (вместе со второй точкой), к которой относится удалённая точка.

```ts
interface LineNode {
    id: string; // идентификатор точки
    isMovable: boolean; // можно ли точку двигать?
    viewOptions: { // параметры внешнего вида
        pointLabel?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            type: 'plain' | 'base64';
            text: string; // текст подписи
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        }
        point?: { // настройки внешнего вида самой точки
            background?: string; // цвет фона
            radius?: number;
            correct?: { // когда точка является правильным ответом
                background?: string;
                radius?: number;
            },
            incorrect?: { // когда точка является неправильным ответом
                background?: string;
                radius?: number;
            }
        }
    },
    projection?: { // проекции точки на оси
        x?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false; // `false` отключает отображение проекции для конкретной точки, если задано отображение проекций для всех точек в `generalViewOptions`
        y?: {
            color?: string;
            thickness?: number;
            type?: 'dashed' | 'dotted' | 'line';
        } | false;
    },
    pos: [ number, number ] // координаты точки
}
```

```ts
interface ILine {
    id: string;
    viewOptions?: {
        line?: { // стиль линии
            color?: string;
            thickness?: number;
            type?: 'line' | 'dotted' | 'dashed'; // line по-умолчанию
            correct?: { // когда линия является правильным ответом
                color?: string;
                thickness?: number;
                type?: 'line' | 'dotted' | 'dashed';
            },
            incorrect?: { // когда линия является неправильным ответом
                color?: string;
                thickness?: number;
                type?: 'line' | 'dotted' | 'dashed';
            }
        },
    }
    points: [LineNode, LineNode]
}
```

```ts
interface Lines {
    type: 'lineSegment' | 'infiniteLine' | 'beam'; // отрезок, линия, луч (из первой точки) - это глобальный параметр для всех построений в рамках текущего объекта механики.
    actions?: {
        addPoint?: 'Click' | 'DoubleClick'; // Тип триггера действия добавления точки
        removePoint?: 'Click' | 'DoubleClick'; // Тип триггера действия удаления точки
    },
    generalViewOptions: { // общие параметры отображения
        pointLabel?: { // настройки подписи
            offset?: [ number, number ] // смещение позиции подписи (верхнего левого угла подписи) относительно центра точки
            fontSize?: number; // размер шрифта
            color?: string; // цвет подписи
            italicQ?: boolean; // курсив?
            boldQ?: boolean; // жирный?
        }
        line?: { // стиль линии
            color?: string;
            thickness?: number;
            type?: 'line' | 'dotted' | 'dashed'; // line по-умолчанию
            correct?: { // когда линия является правильным ответом
                color?: string;
                thickness?: number;
                type?: 'line' | 'dotted' | 'dashed';
            }
            incorrect?: { // когда линия является неправильным ответом
                color?: string;
                thickness?: number;
                type?: 'line' | 'dotted' | 'dashed';
            }
        },
        point?: { // настройки внешнего вида самой точки
            background?: string; // цвет фона
            radius?: number; // радиус "точки"
            correct?: { // когда точка является правильным ответом
                background?: string;
                radius?: number;
            }
            incorrect?: { // когда точка является неправильным ответом
                background?: string;
                radius?: number;
            }
        },
        projection?: { // проекции точки на оси
            x?: {
                background?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line'; // dashed по-умолчанию
            },
            y?: {
                background?: string;
                thickness?: number;
                type?: 'dashed' | 'dotted' | 'line'; // dashed по-умолчанию
            }
        }
    },
    lines: Array<ILine>;
}
```

Объект ответа `checkAnswer.data` представляет собой массив объектов `ILine`, каждый из которых в свою очередь содержит информацию о соответствующих точках:

```ts
interface ICheckAnswer {
    actionId: string;
    data: {
        [inputGraphicsId: string]: Array<ILine>;
    }
}

interface ILine {
    id: string; // идентификатор линии
    points: [ IPoint, IPoint ]; // точки линии (всегда две)
}

interface IPoint {
    id: string; // идентификатор точки
    pos: [ number, number ]; // позиция точки
}
```

Результат проверки ответа:

```ts
interface ICheckAnswerResult {
    actionId: string;
    checkResult: {
        [inputGraphicsId: string]: {
            status: boolean;
            detailed: Array<ILineStatus>;
        }
    },
    replace?: {
        [elementId: string]: IWebPreview;
    }
}

interface ILineStatus {
    id: string;
    status: boolean;
    points: [
        IPointStatus,
        IPointStatus
    ]
}

interface IPointStatus {
    id: string;
    status: boolean;
}
```

`ICheckAnswer.checkResult.[inputGraphicsId].status` отражает общий статус "правильности" ответа: например, все присланные линии оказались правильными, но были присланы не все линии, составляющие верный ответ, тогда результат проверки будет следующим:

```json
{
    "actionId": "ActionId",
    "checkResult": {
        "inputGraphicsId": {
            "status": false,
            "detailed": [
                {
                    "id": "lineId1",
                    "status": true,
                    "points": [
                        { "id": "pointId11", "status": true },
                        { "id": "pointId12", "status": true },
                    ]
                },
                {
                    "id": "lineId2",
                    "status": true,
                    "points": [
                        { "id": "pointId21", "status": true },
                        { "id": "pointId22", "status": true },
                    ]
                }
            ]
        }
    }
}
```

#### LabeledGraphics

Механика для добавления произвольного статичного элемента на график с полем ввода или выбора из списка подписи к элементу. Подпись может быть как простым текстом, так и формулой. На проверку отправляются идентификаторы элементов графика и текст подписей к ним. Учитывая, что подпись, в общем случае, представляет собой математическую формулу - передаётся на проверку в формате base64-закодированной строки.

```ts
interface ILabeledElement {
    id: string; // идентификатор элемента
    element: IAdditionalElement; // см. IAdditionalElement интерфейс выше (в начале документа, слой статичных элементов)
    viewOptions?: {
        label?: {
            color?: string;
            fontSize?: number;
            hAlignment?: 'left' | 'center' | 'right'; // left по-умолчанию
            vAlignment?: 'top' | 'center' | 'bottom'; // bottom по-умолчанию
            correct?: {
                color?: string;
                fontSize?: number;
            },
            incorrect?: {
                color?: string;
                fontSize?: number;
            }
        }
    },
    label: {
        type: 'input' | 'select'; // поле ввода или выпадающий список
        inputType?: 'math' | 'raw'; // тип поля ввода (математическая формула, простая строка), raw по-умолчанию
        selectOptions?: Array<ISelectInput>; // пункты выпадающего списка строк в формате base64
        position: [ number, number ]; // координаты левого верхнего угла блока подписи
        keyboard?: Record<string, unknown>; // объект параметров экранной клавиатуры для текущего поля ввода, переопределяет верхнеуровневый (подробнее в доке mcnmo_math-editor_npm)
    }
}

interface ISelectInput {
    type: 'math' | 'raw';
    value: string; // base64-закодированная строка, если формула; иначе - простая строка, "как есть".
}

interface ILabeledGraphicsInitial {
    generalViewOptions: {
        label?: {
            color?: string;
            fontSize?: number;
            hAlignment?: 'left' | 'center' | 'right'; // left по-умолчанию
            vAlignment?: 'top' | 'center' | 'bottom'; // bottom по-умолчанию
            correct?: {
                color?: string;
                fontSize?: number;
            },
            incorrect?: {
                color?: string;
                fontSize?: number;
            }
        }
    };
    keyboard?: Record<string, unknown> // объект параметров экранной клавиатуры для всех полей ввода (подробнее в доке mcnmo_math-editor_npm)
    elements: Array<ILabeledElement>;
}
```

После заполнения учеником подписей, на проверку отправляется следующая структура данных:

```ts
interface ICheckAnswer {
    actionId: string;
    data: {
        [inputGraphicsId: string]: Array<ILabeledGraphicsAnswer>; // список объектов с идентификатором элемента и его подписью.
    }
}

interface ILabeledGraphicsAnswer {
    id: string;
    label: string; // base64-закодированная строка
}
```

Результат проверки реализует интерфейс:

```ts
interface ICheckAnswerResult {
    actionId: string;
    checkResult: {
        [inputGraphicsId: string]: {
            status: boolean;
            detailed: Array<ILabeledGraphicsStatus>;
        }
    },
    replace?: {
        [elementId: string]: IWebPreview;
    }
}

interface ILabeledGraphicsStatus {
    id: string;
    status: boolean;
}
```

### graphicsOptions: параметры координатной плоскости

#### Значения координатной плоскости

В целом каждый параметр настройки координатной плоскости помечен комментарием и смысл должен быть ясен. Следует остановиться только на нескольких особенностях.

Поле `graphicsOptions.roundingCoordinates` делает координатную плоскость дискретной, то есть мы не можем точку поместить в любое место плоскости - её положение всегда округляется с точностью до того значения, которое здесь передаёся. Если значение не передано или имеет значение `null` - значит работаем с "непрерывной" координатной плоскостью. Важно при этом иметь ввиду, что округление может не совпадать с видимой координатной сеткой, в общем случае.

Параметр может иметь числовое значение, в таком случае, задаваемый шаг округления по всем осям - регулярный. Можно его определить в качестве объекта с ключами `x` и `y` которые содержат число - в таком случае можно для разных осей задать разный шаг округления. И наконец для каждой оси можно мередать массив чисел - они задают явным образом массив значений, до которых должны округляться абцисса и ордината точки.

**В mvp реализуем число.**

#### Координаты и координатные оси

Координатные оси не всегда могут пересекаться в точке `[0, 0]`. Для этого имеется настройка `graphicsOptions.axesOrigin`, в которой указываются значения, в которых оси пересекаются. При этом важно понимать, точка начала отсчёта координатной плоскости, в общем случае, может быть как внутри svg "картинки", так и на границе, равно как и за её пределами и быть недоступной визуально.

При этом важно понимать, что на вход всегда будут приходить "математически правильные" координаты. То есть координатная плоскость формируется от нижнего левого угла направо и вверх, как это принято в математике. При этом координатная плоскость изображения всегда имеет начало координат `[0, 0]` в верхнем левом углу изображения, и идет направо и вниз. Это означает, что все переданные значения нужно предварительно конвертировать в координаты на изображении. При фиксации новых координат объектов, выставленных пользователем - выполнять обратное преобразование координат на изображении в математические координаты.

#### Зум графика и сдвиг курсором

Опираясь на параметр `viewBox` корневого элемента svg можно реализовать масштабирование и смещение графика. Это может быть полезным для работы на мобильных устройствах. Хотелось бы эту опцию иметь, но она не в приоритете. Если окажется, что её реализация занимает слишком много ресурсов - можно отложить. Первое время будем работать с фиксированными по размеру графиками.

По-умолчанию `viewBox` содержит диапазон от 0 до того значения, которое характеризует `graphicsOptions.plotRage` диапазон графика.

#### Координатная сетка и засечки на осях

Важно понимать, что они могут не совпадать в общем случае. Разные линии сетки могут иметь разный стиль отображения. То же самое про засечки на осях. Поэтому для обоих типов настройки графика предусмотрен объект `graphicOptions[gidLines|axesTicks].generalViewOptions` - который описывает общие стили для сетки/засечек. Каждый объект линии сетки/засечки принимает массив значений, на которых элементы располагаются, и если для них указаны свои стилевые особенности - они переопределяют стили из `generalViewOptions` объекта.

### AdditionalElements: дополнительные статичные элементы графика

Это элементы, с которыми взаимодействие не предполагается. Они просто есть в фоне и играют вспомогательную роль. Например, есть задача разметить точки локальных/глобальных максимумом/минимумов функции. Такая задача графически будет представлять: координатную плоскость, статичную линию графика, и над ней расположены кликабельные/подвижные точки, с которыми пользователь уже взаимодействует, формируя ответ.

Поле `additionalElements` это массив объектов, описывающих различные примитивы, которые нужно отбразить на графике. Предварительно кажется, что поддержки следующих примитивов будет более чем достаточно:

_ `text` - статичный текст на графике
- `circle` - статичные точки с подписью и без, а также круги и окружности
- `sector` - статичный сектор окружности
- `rect` - квадраты, с подписью и без
- `line` - статичные линии, с подписями и без
- `arrow` - статичная стрелка, с подписью и без
- `polyline` - статичная ломаная линия (при мелких секциях можно строить гладкие графики функций)
- `polygon` - статичная область на графике
- `path` - более сложные фигуры (пока не предвидится использование, но может пригодиться)
- `image` - вставка растровой картинки

Важно иметь ввиду следующее: нужно каким-то образом сортировать элементы по площади, чтобы не происходило перекрытия. Скорее всего это нужно делать на этапе формирования данных для графика, км сам располагает элементы послойно так, чтобы они друг-друга не закрывали.

#### text

```ts
interface IText {
    pos: [ number, number ]; // позиция верхнего левого угла текста
    type: 'plain' | 'base64';
    text: string;
    color?: string;
    fontSize?: number;
    italicQ?: boolean;
    boldQ?: boolean;
    rotate?: number; // поворот текст относительно своего верхнего левого угла
}

interface IAdditionalText {
    type: 'text';
    options: IText;
}
```

#### circle

```ts
interface ICircle {
    pos: [ number, number ]; // координаты центра окружности
    radius: number; // радиус
    background?: string; // заливка
    border?: { // свойства границы
        type: 'line' | 'dashed' | 'dotted';
        color: string;
        thickness: number;
    },
    projection?: {
        x: {
            color?: string;
            thickness?: number;
            type: 'dashed' | 'dotted' | 'line';
        },
        y: {
            color?: string;
            thickness?: number;
            type: 'dashed' | 'dotted' | 'line';
        }
    }
}

interface IAdditionalCircle {
    type: 'circle';
    options: ICircle;
}
```

В svg нет примитива "точки", поэтому для проставления точек будут использоваться "circle".

#### sector

```ts
interface ISector {
    pos: [ number, number ]; // коодинаты центра
    radius: number; // радиус сектора
    sector: [ number, number ]; // первый и второй уго сектора [ 0, 2Pi] можно для простоты предусмотреть псевдопеременную Pi.
    background?: string; // заливка
    border?: { // свойства границы
        type: 'line' | 'dashed' | 'dotted';
        color: string;
        thickness: number;
    }
}

interface IAdditionalSector {
    type: 'sector';
    options: ISector;
}
```

В svg формате нет примитива "сектор", сектор необходимо реализовать на основе `path` примитива.

#### rect

```ts
interface IRect {
    pos: [ number, number ];
    height: number;
    width: number;
    background?: string;
    rotate?: number; // вращение относительно верхнего левого угла
    border?: {
        type: 'line' | 'dashed' | 'dotted';
        color: string;
        thickness: number;
    }
}

interface IAdditionalRect {
    type: 'rect';
    options: IRect;
}
```

#### line

```ts
interface ILine {
    pos: [
        [ number, number ],
        [ number, number ]
    ];
    thickness?: number;
    type?: 'line' | 'dotted' | 'dashed';
    color?: string;
}

interface IAdditionalLine {
    type: 'line';
    options: ILine;
}
```

#### arrow

```ts
interface IArrow {
    pos: [
        [ number, number ],
        [ number, number ]
    ];
    arrowHeadSize?: [ number, number ];
    thickness?: number;
    type?: 'line' | 'dotted' | 'dashed';
    color?: string;
}

interface IAdditionalArrow {
    type: 'arrow';
    options: IArrow;
}
```

#### polyline

Данный примитив полезен для построения графиков функций, на вход просто передаются значения функции на координатной плоскости. Чем выше плотность значений, тем глаже график.

```ts
interface IPolyline {
    pos: Array<[ number, number ]>;
    thickness?: number;
    type?: 'line' | 'dotted' | 'dashed';
    color?: string;
}

interface IAdditionalPolyline {
    type: 'polyline';
    options: IPolyline;
}
```

Поле `labels` содержит массив подписей к отрезкам ломаной. Чтобы проставить подписи всем отрезкам - длина массива должна быть равна количеству отрезков. Могут быть пустые объекты - в таком случае для данного отрезка подпись не проставляется.

#### polygon

Примитив полезен для обозначения областей графика.

```ts
interface IPolygon {
    pos: Array<[ number, number ]>;
    border?: {
        thickness?: number;
        type?: 'line' | 'dotted' | 'dashed';
        color?: string;
    },
    background?: string;
}

interface IAdditionalPolygon {
    type: 'polygon';
    options: IPolygon;
}
```

Если необходимо проставить подписи к выделяемой области, то КМу следует самому проставить `circle` объекты там, где это необходимо.

#### image

```ts
interface IImage {
    pos: [ number, number ]; // координаты верхнего левого угла изображения
    href: string;
    width: number;
    height: number;
}

interface IAdditionalImage {
    type: 'image';
    options: IImage;
}
```

### Экспорт графика

Заказчик просил также предусмотреть возможность сохранить полученный график в виде статичного svg объекта. Поэтому стоит ко всем механикам такого вида предусмотреть кнопку экспорта, где кусок DOM-дерева, описывающий график передаётся как строка. Или же сразу в виде svg элемента, сгенерированного на стороне фронта. Функционал нужен только для внутреннего пользования, поэтому должен быть отключаемым.


### Настройки

Элемент графических механик поддерживают многочисленные настройки, с помощью которых можно задать вид большинства примитивов и общее поведение механик по умолчанию. Эти настройки механики получают через объект `graphicsOptions` настроек модуля предпросмотра при монтировании компонента.

```ts
interface IGraphicsSettings {
    // Задаёт способ масштабирования размерных параметров, не являющихся частью геометрии задачи.
    // 'image' — масштабирование под пиксели изображения
    // 'original' — размеры применяются "как есть"
    // По умолчанию: 'image'
    sizeMode?: 'image' | 'original';

    // Масштаб шрифтов подписей, размер которых не задан явно или не получен из других параметров и настроек.
    // Для таких подписей размер шрифта задаётся как `contentSize * defaultFontSizeScale`
    // По умолчанию: 0.05
    defaultFontSizeScale?: number;

    // Максимальный временной интревал в миллисекундах по прошествии которого срабатывает двойной клик/тап.
    // По умолчанию: 500
    doubleTapInterval?: number;

    // Пропорциональный размер головки стрелки — её длина и ширина.
    // Итоговый размер головки стрелки будет пропорционален данному параметру и thickness линии
    // Применяется для стрелок, у которых данный парамерт не задан явно (например координатные оси).
    // По умолчанию: [5, 4]
    arrowHeadSize?: [number, number];

    // Настройки библиотеки MathJax
    mathJax?: {
        // Коэффициент масштабирования формул
        // По умолчанию: 0.05
        scaleFactor?: number;

        // URL скрипта MathJax, который будет использован для отображения формул
        // По умолчанию: 'https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js'
        src?: string;
    };

    // Параметры для формирования `strokeDashArray` для разного типа линий
    // `strokeDashArray` в общем случае будет сформирован по паттерну `{dashSize} {dashSpace}`.
    stroke?: {
        // Параметры для линий, не относящихся ни к одной из категорий, перечисленных ниже
        //
        // Размер чёрточки для линии типа "dashed"
        // По умолчанию для `dashSize` всех категорий: 15 
        dashSize: number;
        // Размер пустого пространства для линии типа "dashed"
        // По умолчанию для `dashSpace` всех категорий: 15
        dashSpace: number;
        // Размер точки для линии типа "dotted"
        // По умолчанию для `dotSize` всех категорий: 1
        dotSize: number;
        // Размер пустого пространства для линии типа "dotted"
        // По умолчанию для `dotSpace` всех категорий: 3
        dotSpace: number;

        // Статические линии: ILine, IPolyline
        static: {
            dashSize: number;
            dashSpace: number;
            dotSize: number;
            dotSpace: number;
        },
        // Контуры статических элементов: IRect, ICircle, ISector, IPolygon 
        staticBorders: {
            dashSize: number;
            dashSpace: number;
            dotSize: number;
            dotSpace: number;
        },
        // Линии сетки
        gridLines: {
            dashSize: number;
            dashSpace: number;
            dotSize: number;
            dotSpace: number;
        },
        // Проекции на оси
        projection: {
            dashSize: number;
            dashSpace: number;
            dotSize: number;
            dotSpace: number;
        },
    };

    // Показывать ли кнопку для скачивания SVG
    // По умолчанию: false
    exportQ?: boolean;

    // Разрешить ли зум по умолчанию для всех механик
    // По умолчанию: true
    zoomQ?: boolean;
    // настройки зума и панели управления зумом
    zoomControls?: {
        // Позиция панели управления зумом
        // По умолчанию: ['right', 'bottom']
        position?: ['left' | 'right', 'top' | 'bottom' ];
        
        // Масимальный масштаб приближения
        // По умолчанию: 3
        maxZoom?: number;

        // Шаг зума
        // По умолчанию: 0.5
        zoomStep?: number;
    };
    // Настройки камеры для зума и перемещнию по графику
    camera?: {
        touchMoveInterval?: camera.touchMoveInterval ?? 15,

        // Скорость смещения графика при зуме
        // По умолчанию: 4
        panSpeed?: number;

        // Коэффиент скорости смещения графика при зуме для touch-устройств
        // По умолчанию: 0.035
        panSpeedMultiplierTouch?: number;

        // Интервал срабатывания смещения графика при зуме
        // По умолчанию: 16
        panInterval?: number;
    };

    // Проекции и координаты курсора
    cursor?: {
        // 
        projection?: {
            color?: string; // По умолчанию: 'grey'
            thickness?: number // По умолчанию: 2
            type?: 'line' | 'dotted' | 'dashed' // По умолчанию: 'dashed';
        };
        coordinates?: {
            // Количество отображаемых цифр послея запятой в координатах курсора
            // По умолчанию: 2
            roundDigits?: number;
            fontSize?: number; // По умолчанию: 20
            color?: cursor.coordinates?.color; // По умолчанию: 'black'
            boldQ?: cursor.coordinates?.boldQ; // По умолчанию: false
            italicQ?: cursor.coordinates?.italicQ;// По умолчанию: false
        };
    };

    // Настройки слоёв элементов
    layers?: {
            /*
                Список статических элементов, которые будут "обрезаны" по plotRange
                По умолчанию:
                [
                    'circle',
                    'sector',
                    'rect',
                    'line',
                    'arrow',
                    'polyline',
                    'polygon',
                    'path',
                    'image'
                ]
            */
            staticElementsClipped?: Array<IAnyStaticElement.type>;
            /*
                Список статических элементов, которые не будут обрезаны.
                По умолчанию:
                [
                    'text'
                ]

                Каждый тип статический элемента должен присутствовать РОВНО в 1 списке,
                иначе он будет отрисован несколько раз или не отрисован вообще.
            */
            staticElementsUnclipped?: Array<IAnyStaticElement.type>;
    };
    // Паддинги изображения, на которые будет увеличен `contentSize`,
    // пространство в которых можно использовать для отображения подписей.
    // По умолчанию: 0
    padding?: number | [number, number] | [number, number, number] | [number, number, number, number];

    // Фон механики
    // По умолчанию: '#fff';
    background?: string;
    coordinateSystem?: 'decart' | 'affine' | 'polar'; // По умолчанию: 'decart'
    chart?: {
        // Рамка вокруг контентной области (графика)
        border?: {
            width?: number; // По умолчанию: 0 — рамка отсутствует
            color?: string; // По умолчанию: 'black'
        }
    };

    // Подпись к графику снизу и сверху
    graphicsLabel?: {
        color?: string; // По умолчанию: 'black'
        fontSize?: string; // По умолчанию: 20
        italicQ?: string; // По умолчанию: false
        boldQ?: string; // По умолчанию: false
    };

    // Подпись начала координат
    graphicsLabel?: {
        pos?: ['left' | 'right', 'top' | 'bottom']; // По умолчанию: ['left', 'bottom']
        color?: string; // По умолчанию: 'black'
        fontSize?: string; // По умолчанию: 8
        italicQ?: string; // По умолчанию: false
        boldQ?: string; // По умолчанию: false
    };

    // Группа настроек отображения разных категорий линий по умолчанию

    // Линии сетки
    gridLines?: {
        type?: 'line'|'dashed'|'dotted'; // По умолчанию: 'dashed'
        color?: string; // По умолчанию: 'grey'
        thickness?: number; // По умолчанию: 1.5
    };

    // Статические линии: ILine, IPolyline
    staticLines?: {
        type?: 'line'|'dashed'|'dotted'; // По умолчанию: 'line'
        color?: string; // По умолчанию: 'grey'
        thickness?: number; // По умолчанию: 1.5
    };
    
    // Контуры статических элементов: IRect, ICircle, ISector, IPolygon 
    staticBorders?: {
        type?: 'line'|'dashed'|'dotted'; // По умолчанию: 'line'
        color?: string; // По умолчанию: 'grey'
        thickness?: number; // По умолчанию: 1
    } | false; // false - контуры не отображаются

    // Проекции
    projections?: {
        type?: 'line'|'dashed'|'dotted'; // По умолчанию: 'dashed'
        color?: string; // По умолчанию: 'darkgrey'
        thickness?: number; // По умолчанию: 1
    };

    // Отображение осей
    axis?: {
        ticks?: {
            type?: 'dash' | 'dot' | 'emptyCircle'; // По умолчанию: 'dash'
            color?: string; // По умолчанию: 'black;
            dotRadius?: number; // По умолчанию: 7
            dashLength?: number; // По умолчанию: 18
            dashWidth?: number; // По умолчанию: 2.5
            emptyCircleStrokeWidth?: number; // По умолчанию: 1.85
            emptyCircleRadius?: number; // По умолчанию: 7
        };
        ticksLabels?: {
            fontSize?: number; // По умолчанию: 20
            color?: string; //По умолчанию: 'black'
            pos?: { // позиция относительно соответствующей оси
                vertical: 'left' | 'right'; // По умолчанию: 'left'
                horizontal: 'top' | 'bottom'; // По умолчанию: 'bottom'
            }
        };
        thickness?: number; // По умолчанию: 2
        color: string; // По умолчанию: 'black'
        label: { // Подпись оси
            fontSize: number; // По умолчанию: 20
            color: string; // По умолчанию: 'black'
            boldQ: boolean; // По умолчанию: false
            italicQ: boolean; // По умолчанию: false
        }
    };

    // Настройки отображения интерактивных элементов
    interactions?: {
        // Минимальная величина в пикселях, на которую можно переместить точку или сдвинуть график при зуме.
        // Смещения меньшей величины расцениваются как клики, которые  для динамических механик добавляют/удалят элементы.
        // По умолчанию: 5
        clickMovementThreshold?: number;

        actions?: {
            // Тип триггера действия добавления элемента для механик с динамическим количеством элементов (например, динамические точки)
            // По умолчанию: 'Click'
            addElementAction: 'Click' | 'DoubleClick';
            // Тип триггера действия удаления элемента для механик с динамическим количеством элементов
            // По умолчанию: 'DoubleClick'
            removeElementAction: 'Click' | 'DoubleClick';
        },

        pointProjection?: {
            type?: 'line'|'dashed'|'dotted'; // По умолчанию: 'dashed'
            thickness?: interactions?.pointProjection?.thickness ?? 2,
            color?: interactions?.pointProjection?.color ?? 'grey',
        },
        line: { // Линия в механиках IMovablePointsLines, IDynamicMovablePointsLines
            type: 'line'|'dashed'|'dotted'; // По умолчанию: 'line'
            thickness: number; // По умолчанию: 5
            color: string; // По умолчанию: 'grey'
            labelCircleRadius: // Радиус круга, в котором отображается подпись линии. По умолчанию: 30
        },
        label?: { // Подпись к точке
            color?: string; // По умолчанию: 'black'
            fontSize?: number; // По умолчанию: 24
        },
        correct?: { // Настройки "правильной" точки.
            color?: string; // По умолчанию: #8EBC33
            pointRadius?: interactions?.correct?.radius ?? 7.5
        },
        incorrect?: {
            color?: string; // По умолчанию: #8EBC33
            pointRadius?: number; // По умолчанию: 7.5
        },
        checked?: { // Точка выбрана
            color?: string; // По умолчанию: 'blue'
            pointRadius?: number; // По умолчанию: 7.5
        },
        unchecked?: { // Точка не выбрана
            color?: string; // По умолчанию: 'grey',
            pointRadius?: number; // По умолчанию: 7.5
        },
        untouched?: { // Начальное состояние точки
            color?: string; // По умолчанию: 'grey',
            pointRadius?: number; // По умолчанию: 7.5
        }
    }
}
```