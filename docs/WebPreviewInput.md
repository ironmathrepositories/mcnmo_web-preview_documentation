# WebPreviewInput

WebPreviewInput — компонент текстового ввода с поддержкой подсветки фрагментов текста (например, синтаксиса TEX) и проверки орфографии.

Компонент состоит из поля ввода и верхней панели инструментов, которая включает в себя кнопку для активации режима проверки орфографии и переключатель подстветки синтаксиса. Панель инструментов может быть расширена в будущем.

## Использование

### npm

```ts
import { WebPreviewInput } from 'mcnmo_web-preview';
import 'mcnmo_web-preview/lib/main.css';

const props = {
    /* ... */
};
render(<WebPreviewInput {...props} />);
```

### Module federation

```ts
const WebPreviewInputRemote = React.lazy(() => import('web_preview/WebPreviewInput'));

export const WebPreviewInput = React.forwardRef((props: WebPreviewInputProps, ref): JSX.Element => {
    return (
        <React.Suspense fallback={null}>
            <WebPreviewInputRemote {...props} ref={ref} />
        </React.Suspense>
    );
});
WebPreviewInput.displayName = 'WebPreviewInput';
```

## API

Компонент `<WebPreviewInput>` принимает следующие параметры:

```ts
interface WebPreviewInputProps {
    // изначальный текст в поле ввода
    initialText?: string;

    // отключена ли возможность изменения текста
    disabled?: boolean;

    // событие, срабатывающее при изменении текста в поле ввода
    onChange?(newText: string): void;

    controls?: WebPreviewInputControls;

    // Подсвечивать ли фрагменты изначально
    highlight?: boolean; // default = true
    // Конфигурация подсветки фрагментов
    highlightFragments?: Array<HighlightBlock>;

    // использовать ли встроенную в браузер проверку орфографии
    builtinSpellCheck?: boolean; // default = false
    spellCheckProvider: 'Yandex' | 'LanguageTool' | 'YandexWithFallback';

    // коллбэк, который можно переопределить функцию для проверки орфографии
    onSpellCheck?(textBlocks: string[]): Promise<Array<Array<SpellCheckResult>>>;

    // CSS класс корневого элемента
    className?: string;
    // CSS класс текстовых блоков с ошибкой
    errorClassName?: string;

    // Разделитель блоков текста
    // Может рабоать нестабильно с проверкой орфографии,
    // пока не рекомендуется к использованию
    blockDelimeter?: string; // default = '\\UNSTABLE_NEW_BLOCK'
}
```

## Панель инструментов

Параметр `controls` компонента отвечает за отображение и активность панели инструментов.

```ts
interface InstrumentButton {
    visible: boolean;
    disabled: boolean;
}

interface WebPreviewInputControls {
    spellCheck: InstrumentButton;
    highlight: InstrumentButton;
}
```

## Проверка орфографии

Компонент реализует проверку орфографии через обращение к сторонним сервисам. Поддерживаемые сервисы:

-   [Яндекс.Спеллер](https://yandex.ru/dev/speller/)
-   [LanguageTool](https://languagetool.org/ru)
-   `Яндекс.Спеллер` с `LanguageTool` в качестве фоллбэка (при ответе с ошибкой от `Яндекс.Спеллер`, например, при превышении лимита запросов).

Компонент принимает опциональный асинхронный коллбэк `onSpellCheck`, который будет вызван для проверки орфографии и может содержать произвольную логику и/или обращение к другим сервисам. Ожидаемый результат работы коллбэка — массив с ошибками для каждого текстового блока `Array<Array<<SpellCheckResult>>`.

```ts
interface SpellCheckResult {
    pos: number; // позиция слова с ошибкой в тексте
    len: number; // длина слова с ошибкой
    s: string[]; // предложения для исправления ошибки
}
```

## Подсветка синтаксиса

Компонент принимает параметр `highlightFragments`, который позволяет задавать при помощи регулярных выражений
фрагменты синтаксиса для подсветки и визуальные параметры, применяемые к этим фрагментам.

```ts
interface HighlightFragment {
    // тип фрагмента - произвольный идентификатор
    type: string;
    // регулярное выражение, задающее фрагмент
    test: RegExp;
    // CSS стили, применяемые к фрагменту (color, background-color, ...)
    style: CSSProperties;
}
```

## Стилизация

Внешний вид компонент может быть изменён при помощи глобальных CSS классов:

-   DraftEditor-root
-   DraftEditor-editorContainer
-   public-DraftEditor-content
-   public-DraftStyleDefault-block

Пример стилизации поля ввода:

`styles.module.css`:

```css
.CustomEditor {
    padding: 10px;

    :global(.DraftEditor-editorContainer) {
        border: 2px solid rebeccapurple;
    }
}
```

`main.ts`:

```ts
import styles from './styles.module.css';

<WebPreviewInput className={styles.CustomEditor} />;
```
