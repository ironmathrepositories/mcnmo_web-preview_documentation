# Использование модуля

* Установить зависимость в проект командой `npm i mcnmo_web-preview`
* Импортировать компонент модуля в проект `import TexWebPreview from 'mcnmo_web-preview';`
* Создать инстанс компонента и передать необходимые пропсы:
    * `data` объект данных для предпросмотра, интерфейс описан ниже
    * `checkAnswerHandler` функция для проверки ответов, дока по функции будет ниже
    * `externalStyles` объект внешних стилей (необязательный параметр)
    * `settings` - объект глобальных настроек модуля
    * `initialInputsState` - начальное состояние полей ввода.
* Done!

# Документация по использованию модуля

[Документация](https://bitbucket.org/ironmathrepositories/mcnmo_web-preview_documentation/src/documentation/docs/README.md)