## NPM scripts

#### `npm run build` Собирает module federation container в ./lib-module.

#### `npm run build:dev` Заускает dev-server с module federation контейнером, доступным по адресу http://localhost:5000

#### `npm run build:npm` Собирает bundle для npm в ./lib.
#### `npm run deploy:test` Выгружает собранную в module federation формате сборку на http://altmpp.mathem.ru

#### `npm run deploy:prod` Выгружает собранную в module federation формате сборку на http://mpp.mathem.ru